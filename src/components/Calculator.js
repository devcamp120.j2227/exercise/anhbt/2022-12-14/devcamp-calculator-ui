import React from 'react'
import styled from 'styled-components'

const CalculationArea = styled.div`
text-align: right;
color: #AEB3BA;
padding-top: .3em;
padding-bottom: .2em;
border-bottom: solid white 1px
`
const ResultArea = styled.div`
color: white;
text-align: right;
font-size: 1.5em;
padding-top: .6em;
padding-bottom: .5em;
`
const CalculatorButton = styled.button`
background: ${props => props.muted ? "#404D5E" : "#425062"};
color: ${props => props.color || (props.muted ? "#AEB3BA" : "#FFFFFF")};
text-align: center;
border: none;
height: 40px;
width: 40px;
`
const ButtonGrid = styled.div`
display: grid;
grid-template-columns: repeat(4, 40px);
gap: 2px;
`

const CalculatorWrapper = styled.div`
display: flex;
flex-direction: column;
width: 165px;
background: #3A4655;
padding: 3px;
;
`
function Calculator() {
  return (
    <CalculatorWrapper >
        <CalculationArea>2536 + 419 + </CalculationArea>
        <ResultArea>2955</ResultArea>
        <ButtonGrid>
            <CalculatorButton color='#BF535C' muted>C</CalculatorButton>
            <CalculatorButton muted>≠</CalculatorButton>
            <CalculatorButton muted>%</CalculatorButton>
            <CalculatorButton muted>/</CalculatorButton>
            
            <CalculatorButton>7</CalculatorButton>
            <CalculatorButton>8</CalculatorButton>
            <CalculatorButton>9</CalculatorButton>
            <CalculatorButton muted>x</CalculatorButton>

            <CalculatorButton>4</CalculatorButton>
            <CalculatorButton>5</CalculatorButton>
            <CalculatorButton>6</CalculatorButton>
            <CalculatorButton muted>-</CalculatorButton>
            
            <CalculatorButton>1</CalculatorButton>
            <CalculatorButton>2</CalculatorButton>
            <CalculatorButton>3</CalculatorButton>
            <CalculatorButton muted>+</CalculatorButton>
            
            <CalculatorButton>.</CalculatorButton>
            <CalculatorButton>0</CalculatorButton>
            <CalculatorButton>&lt;</CalculatorButton>
            <CalculatorButton muted>=</CalculatorButton>
        </ButtonGrid>
    </CalculatorWrapper>
  )
}

export default Calculator